{
  description = "Rust devshell";

  inputs = {
    nixpkgs.url      = "github:nixos/nixpkgs/nixos-unstable";
    rust-overlay.url = "github:oxalica/rust-overlay";
    flake-utils.url  = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, rust-overlay, flake-utils, ... }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        overlays = [ (import rust-overlay) ];
        pkgs = import nixpkgs {
          inherit system overlays;
        };
      in
      with pkgs;
      {
        devShell = mkShell rec {
          nativeBuildInputs = [
            pkg-config
          ];
          buildInputs = [
            (rust-bin.selectLatestNightlyWith (toolchain: toolchain.default.override {
              extensions = [ "rust-src" ];
            }))
            xorg.libX11 xorg.libXcursor xorg.libXi xorg.libXrandr # To use the x11 feature
            gtk3
            openssl
            rust-analyzer
            alsa-lib
            vulkan-loader
            udev
            wayland
            libxkbcommon
            just
            mdbook
            eza
            fd
          ];
          LD_LIBRARY_PATH = lib.makeLibraryPath buildInputs;

          shellHook = ''
            alias ls=eza
            alias find=fd
          '';
        };
      }
    );
}
