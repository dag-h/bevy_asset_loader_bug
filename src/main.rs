use bevy::prelude::*;
use bevy::gltf::Gltf;
use bevy_asset_loader::prelude::*;

#[derive(Debug, Clone, Copy, Default, PartialEq, Eq, Hash, States)]
enum AppState {
    #[default]
    Loading,
    Finished,
}

#[derive(AssetCollection, Resource)]
pub struct Models {
    #[asset(path = "models/male.glb")]
    pub male_model: Handle<Gltf>,

    #[asset(path = "models/cube.glb")]
    pub cube_model: Handle<Gltf>,

    #[asset(path = "models/barrel.glb")]
    pub barrel_model: Handle<Gltf>,

    #[asset(path = "models/box.glb")]
    pub box_model: Handle<Gltf>,

    #[asset(path = "models/campfire.glb")]
    pub campfire_model: Handle<Gltf>,

    #[asset(path = "models/chest_1.glb")]
    pub chest_model: Handle<Gltf>,

    #[asset(path = "models/outhouse.glb")]
    pub outhouse_model: Handle<Gltf>,

    #[asset(path = "models/lamppost.glb")]
    pub lamppost_model: Handle<Gltf>,

    #[asset(path = "models/wheelbarrow.glb")]
    pub wheelbarrow_model: Handle<Gltf>,
}

fn main() {
    let mut app = App::new();
    app
        .add_plugins(DefaultPlugins)
        .add_state::<AppState>()
        .add_loading_state(
            LoadingState::new(AppState::Loading).continue_to_state(AppState::Finished),
        )
        .add_systems(OnEnter(AppState::Loading), || {
            info!("Entered loading state");
        })
        .add_systems(OnExit(AppState::Loading), || {
            info!("Exited loading state");
        })
        .add_collection_to_loading_state::<_, Models>(AppState::Loading);
    app.run();
}
